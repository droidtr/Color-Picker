package com.keskin.colorpicker;

import android.app.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import android.content.*;
import com.keskin.colorpicker.*;

public class MainActivity extends Activity 
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		int i=0;
		final ColorPicker cp =new ColorPicker(this);
		setContentView(cp);
		super.onCreate(savedInstanceState);
		cp.okay.setOnClickListener(new OnClickListener(){
			public void onClick(View p1){
				finish();
			}
		});
	}
}
class ColorPicker extends LinearLayout{
	int c=0;
	int cf=0;
	LinearLayout i=null;
	TextView hex=null;
	int a=255;
	Context ctx=null;
	SeekBar alpha=null;
	public Button okay=null;
	ColorPicker(Context context){
		super(context);
		ctx=context;
		i = new LinearLayout(ctx);
		final LinearLayout ix = new LinearLayout(ctx);
		final LinearLayout bottomlayer = new LinearLayout(ctx);
		okay=new Button(ctx);
		hex=new TextView(ctx);
		alpha=new SeekBar(ctx);
		alpha.setMax(255);
		ix.setLayoutParams(new LinearLayout.LayoutParams(-1,-1,1.0f));
		i.setLayoutParams(new LinearLayout.LayoutParams(-1,-1,1.0f));
		bottomlayer.setLayoutParams(new LinearLayout.LayoutParams(-1,-2));
		this.addView(i);
		i.setOrientation(LinearLayout.VERTICAL);
		i.addView(ix);
		i.addView(bottomlayer);
		i.addView(alpha);
		this.setBackgroundColor(Color.BLACK);
		bottomlayer.addView(okay);
		bottomlayer.addView(hex);
		hex.setTextColor(Color.WHITE);
		alpha.setPadding(20,20,20,20);
		bottomlayer.setPadding(20,20,20,20);
		Bitmap bmp = Bitmap.createBitmap(365,511,Bitmap.Config.ARGB_8888);
		for(int k=0;k<365;k++){
			for(int j=0;j<=255;j++){
				bmp.setPixel(k,j,(int)Color.HSVToColor(new float[]{(float)k,(float)j/255f,1f}));
			}
			for(int j=0;j<=255;j++){
				bmp.setPixel(k,j+255,(int)Color.HSVToColor(new float[]{(float)k,1f,(float)(255-j)/255f}));
			}
		}
		ix.setOnTouchListener(new LinearLayout.OnTouchListener(){
			public boolean onTouch(View p1, MotionEvent event){
				switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN:
						case MotionEvent.ACTION_MOVE:
						case MotionEvent.ACTION_UP:
						int x=(int)((365*event.getX())/p1.getWidth());
						int y=(int)((511*event.getY())/p1.getHeight());
						if(x<0){x=0;}else if(x>=365){x=364;}
						if(y<0){y=0;}else if(y>=511){y=510;}
						c=bmp.getPixel(x,y);
						cf=bmp.getPixel(x,255);
						update(a);
				}
				return true;			
			}
		});
		alpha.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				a=progress;
				update(progress);
			}
			
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
		});
		alpha.setProgress(255);
		ix.setBackgroundDrawable(new BitmapDrawable(ctx.getResources(), bmp)); 
	}
	private void update(int aaa){
		c=Color.argb(aaa,Color.red(c),Color.green(c),Color.blue(c));
		GradientDrawable gd = new GradientDrawable();
		gd.setColor(c);
		gd.setCornerRadius(14);
		gd.setStroke(3,Color.WHITE);
		okay.setBackgroundDrawable(gd);
		alpha.getProgressDrawable().setColorFilter(cf, PorterDuff.Mode.SRC_IN);
		alpha.getThumb().setColorFilter(cf, PorterDuff.Mode.SRC_IN);
		hex.setText("    "+String.format("#%08X", 0xFFFFFFFF & c)+"    |    "+Color.alpha(c)+" - "+Color.red(c)+" - "+Color.green(c)+" - "+Color.blue(c));
	}
	public int getColor(){
		return c;
	}
}
